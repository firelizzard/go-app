/*
	Copyright (c) 2019 The Go App Authors

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

package cgo

import (
	"runtime"
	"sync"
	"unsafe"
)

type Reference uintptr

var GlobalScope = new(Scope)

var refs = struct {
	sync.RWMutex
	next Reference
	lup  map[Reference]interface{}
}{
	next: 1,
	lup: map[Reference]interface{}{},
}

func Save(v interface{}) Reference {
	refs.Lock()
	r := refs.next
	refs.next++
	refs.lup[r] = v
	refs.Unlock()

	return r
}

func (r Reference) C() unsafe.Pointer {
	return unsafe.Pointer(r)
}

func (r Reference) Set(v interface{}) interface{} {
	refs.Lock()
	u, _ := refs.lup[r]
	refs.lup[r] = v
	refs.Unlock()
	return u
}

func (r Reference) Load() interface{} {
	refs.RLock()
	v, _ := refs.lup[r]
	refs.RUnlock()
	return v
}

func (r Reference) Delete() {
	refs.Lock()
	delete(refs.lup, r)
	refs.Unlock()
}

func (r Reference) AttachTo(target interface{ AttachReference(Reference) }) Reference {
	target.AttachReference(r)
	return r
}

type Scope struct {
	refs []Reference
}

func (s *Scope) finalize() {
	for _, r := range s.refs {
		r.Delete()
	}
}

func (s *Scope) AttachReference(r Reference) {
	if s == GlobalScope {
		return
	}
	if s.refs == nil {
		runtime.SetFinalizer(s, (*Scope).finalize)
	}
	s.refs = append(s.refs, r)
}

func (s *Scope) Save(v interface{}) Reference {
	return Save(v).AttachTo(s)
}
