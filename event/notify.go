package event

import "sync"

var eventChain = struct {
	sync.RWMutex
	chs []chan interface{}
}{}

func Notify(ch chan interface{}, types ...interface{}) {
	if len(types) > 0 {
		some, all := ch, make(chan interface{})
		ch = all

		go func() {
			for v := range all {
				for _, typ := range types {
					if v == typ {
						some <- v
						break
					}
				}
			}
		}()
	}

	eventChain.Lock()
	eventChain.chs = append(eventChain.chs, ch)
	eventChain.Unlock()
}

func send(v interface{}) {
	eventChain.RLock()
	chs := eventChain.chs
	eventChain.RUnlock()

	for _, ch := range chs {
		ch <- v
	}
}

type Lifecycle int

const (
	Alive Lifecycle = iota
	Visible
	Dead
)

func (v Lifecycle) Send() {
	send(v)
}
