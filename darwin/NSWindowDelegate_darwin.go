/*
Copyright (c) 2019 The Go App Authors

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// THIS FILE IS GENERATED
// DO NOT MODIFY

package darwin

// #import <AppKit/AppKit.h>
// void * newGoWindowDelegate(void * go);
import "C" // DO NOT REMOVE

import (
	"unsafe"

	"gitlab.com/firelizzard/go-app/cgo"
)

func (x *WindowDelegate) AllocAndSave(s *cgo.Scope) (cgo.Reference, unsafe.Pointer) {
	r := cgo.Save(x)
	if s != nil {
		r.AttachTo(s)
	}

	id := C.newGoWindowDelegate(r.C())
	return r, id
}

func (x *WindowDelegate) AllocAndSet(r cgo.Reference) unsafe.Pointer {
	r.Set(x)
	return C.newGoWindowDelegate(r.C())
}

type WindowDelegate struct {
	ShouldClose func(sender NSWindow) bool
	DidFailToEnterFullScreen func(window NSWindow)
	DidFailToExitFullScreen func(window NSWindow)
	WillEncodeRestorableState func(window NSWindow, state NSCoder)
	DidDecodeRestorableState func(window NSWindow, state NSCoder)
	DidResize func(notification NSNotification)
	DidExpose func(notification NSNotification)
	WillMove func(notification NSNotification)
	DidMove func(notification NSNotification)
	DidBecomeKey func(notification NSNotification)
	DidResignKey func(notification NSNotification)
	DidBecomeMain func(notification NSNotification)
	DidResignMain func(notification NSNotification)
	WillClose func(notification NSNotification)
	WillMiniaturize func(notification NSNotification)
	DidMiniaturize func(notification NSNotification)
	DidDeminiaturize func(notification NSNotification)
	DidUpdate func(notification NSNotification)
	DidChangeScreen func(notification NSNotification)
	DidChangeScreenProfile func(notification NSNotification)
	DidChangeBackingProperties func(notification NSNotification)
	WillBeginSheet func(notification NSNotification)
	DidEndSheet func(notification NSNotification)
	WillStartLiveResize func(notification NSNotification)
	DidEndLiveResize func(notification NSNotification)
	WillEnterFullScreen func(notification NSNotification)
	DidEnterFullScreen func(notification NSNotification)
	WillExitFullScreen func(notification NSNotification)
	DidExitFullScreen func(notification NSNotification)
	WillEnterVersionBrowser func(notification NSNotification)
	DidEnterVersionBrowser func(notification NSNotification)
	WillExitVersionBrowser func(notification NSNotification)
	DidExitVersionBrowser func(notification NSNotification)
	DidChangeOcclusionState func(notification NSNotification)
}

func _WindowDelegate_load(ptr unsafe.Pointer) *WindowDelegate {
	if ptr == nil {
		return nil
	}

	v := cgo.Reference(ptr).Load()
	if v == nil {
		return nil
	}

	return v.(*WindowDelegate)
}

//export _WindowDelegate_ShouldClose
func _WindowDelegate_ShouldClose(ptr unsafe.Pointer, sender NSWindow) bool {
	if del := _WindowDelegate_load(ptr); del != nil && del.ShouldClose != nil {
		return del.ShouldClose(sender)
	}
	return defaultWindowDelegateShouldClose
}

//export _WindowDelegate_DidFailToEnterFullScreen
func _WindowDelegate_DidFailToEnterFullScreen(ptr unsafe.Pointer, window NSWindow) {
	if del := _WindowDelegate_load(ptr); del != nil && del.DidFailToEnterFullScreen != nil {
		del.DidFailToEnterFullScreen(window)
	}
}

//export _WindowDelegate_DidFailToExitFullScreen
func _WindowDelegate_DidFailToExitFullScreen(ptr unsafe.Pointer, window NSWindow) {
	if del := _WindowDelegate_load(ptr); del != nil && del.DidFailToExitFullScreen != nil {
		del.DidFailToExitFullScreen(window)
	}
}

//export _WindowDelegate_WillEncodeRestorableState
func _WindowDelegate_WillEncodeRestorableState(ptr unsafe.Pointer, window NSWindow, state NSCoder) {
	if del := _WindowDelegate_load(ptr); del != nil && del.WillEncodeRestorableState != nil {
		del.WillEncodeRestorableState(window, state)
	}
}

//export _WindowDelegate_DidDecodeRestorableState
func _WindowDelegate_DidDecodeRestorableState(ptr unsafe.Pointer, window NSWindow, state NSCoder) {
	if del := _WindowDelegate_load(ptr); del != nil && del.DidDecodeRestorableState != nil {
		del.DidDecodeRestorableState(window, state)
	}
}

//export _WindowDelegate_DidResize
func _WindowDelegate_DidResize(ptr unsafe.Pointer, notification NSNotification) {
	if del := _WindowDelegate_load(ptr); del != nil && del.DidResize != nil {
		del.DidResize(notification)
	}
}

//export _WindowDelegate_DidExpose
func _WindowDelegate_DidExpose(ptr unsafe.Pointer, notification NSNotification) {
	if del := _WindowDelegate_load(ptr); del != nil && del.DidExpose != nil {
		del.DidExpose(notification)
	}
}

//export _WindowDelegate_WillMove
func _WindowDelegate_WillMove(ptr unsafe.Pointer, notification NSNotification) {
	if del := _WindowDelegate_load(ptr); del != nil && del.WillMove != nil {
		del.WillMove(notification)
	}
}

//export _WindowDelegate_DidMove
func _WindowDelegate_DidMove(ptr unsafe.Pointer, notification NSNotification) {
	if del := _WindowDelegate_load(ptr); del != nil && del.DidMove != nil {
		del.DidMove(notification)
	}
}

//export _WindowDelegate_DidBecomeKey
func _WindowDelegate_DidBecomeKey(ptr unsafe.Pointer, notification NSNotification) {
	if del := _WindowDelegate_load(ptr); del != nil && del.DidBecomeKey != nil {
		del.DidBecomeKey(notification)
	}
}

//export _WindowDelegate_DidResignKey
func _WindowDelegate_DidResignKey(ptr unsafe.Pointer, notification NSNotification) {
	if del := _WindowDelegate_load(ptr); del != nil && del.DidResignKey != nil {
		del.DidResignKey(notification)
	}
}

//export _WindowDelegate_DidBecomeMain
func _WindowDelegate_DidBecomeMain(ptr unsafe.Pointer, notification NSNotification) {
	if del := _WindowDelegate_load(ptr); del != nil && del.DidBecomeMain != nil {
		del.DidBecomeMain(notification)
	}
}

//export _WindowDelegate_DidResignMain
func _WindowDelegate_DidResignMain(ptr unsafe.Pointer, notification NSNotification) {
	if del := _WindowDelegate_load(ptr); del != nil && del.DidResignMain != nil {
		del.DidResignMain(notification)
	}
}

//export _WindowDelegate_WillClose
func _WindowDelegate_WillClose(ptr unsafe.Pointer, notification NSNotification) {
	if del := _WindowDelegate_load(ptr); del != nil && del.WillClose != nil {
		del.WillClose(notification)
	}
}

//export _WindowDelegate_WillMiniaturize
func _WindowDelegate_WillMiniaturize(ptr unsafe.Pointer, notification NSNotification) {
	if del := _WindowDelegate_load(ptr); del != nil && del.WillMiniaturize != nil {
		del.WillMiniaturize(notification)
	}
}

//export _WindowDelegate_DidMiniaturize
func _WindowDelegate_DidMiniaturize(ptr unsafe.Pointer, notification NSNotification) {
	if del := _WindowDelegate_load(ptr); del != nil && del.DidMiniaturize != nil {
		del.DidMiniaturize(notification)
	}
}

//export _WindowDelegate_DidDeminiaturize
func _WindowDelegate_DidDeminiaturize(ptr unsafe.Pointer, notification NSNotification) {
	if del := _WindowDelegate_load(ptr); del != nil && del.DidDeminiaturize != nil {
		del.DidDeminiaturize(notification)
	}
}

//export _WindowDelegate_DidUpdate
func _WindowDelegate_DidUpdate(ptr unsafe.Pointer, notification NSNotification) {
	if del := _WindowDelegate_load(ptr); del != nil && del.DidUpdate != nil {
		del.DidUpdate(notification)
	}
}

//export _WindowDelegate_DidChangeScreen
func _WindowDelegate_DidChangeScreen(ptr unsafe.Pointer, notification NSNotification) {
	if del := _WindowDelegate_load(ptr); del != nil && del.DidChangeScreen != nil {
		del.DidChangeScreen(notification)
	}
}

//export _WindowDelegate_DidChangeScreenProfile
func _WindowDelegate_DidChangeScreenProfile(ptr unsafe.Pointer, notification NSNotification) {
	if del := _WindowDelegate_load(ptr); del != nil && del.DidChangeScreenProfile != nil {
		del.DidChangeScreenProfile(notification)
	}
}

//export _WindowDelegate_DidChangeBackingProperties
func _WindowDelegate_DidChangeBackingProperties(ptr unsafe.Pointer, notification NSNotification) {
	if del := _WindowDelegate_load(ptr); del != nil && del.DidChangeBackingProperties != nil {
		del.DidChangeBackingProperties(notification)
	}
}

//export _WindowDelegate_WillBeginSheet
func _WindowDelegate_WillBeginSheet(ptr unsafe.Pointer, notification NSNotification) {
	if del := _WindowDelegate_load(ptr); del != nil && del.WillBeginSheet != nil {
		del.WillBeginSheet(notification)
	}
}

//export _WindowDelegate_DidEndSheet
func _WindowDelegate_DidEndSheet(ptr unsafe.Pointer, notification NSNotification) {
	if del := _WindowDelegate_load(ptr); del != nil && del.DidEndSheet != nil {
		del.DidEndSheet(notification)
	}
}

//export _WindowDelegate_WillStartLiveResize
func _WindowDelegate_WillStartLiveResize(ptr unsafe.Pointer, notification NSNotification) {
	if del := _WindowDelegate_load(ptr); del != nil && del.WillStartLiveResize != nil {
		del.WillStartLiveResize(notification)
	}
}

//export _WindowDelegate_DidEndLiveResize
func _WindowDelegate_DidEndLiveResize(ptr unsafe.Pointer, notification NSNotification) {
	if del := _WindowDelegate_load(ptr); del != nil && del.DidEndLiveResize != nil {
		del.DidEndLiveResize(notification)
	}
}

//export _WindowDelegate_WillEnterFullScreen
func _WindowDelegate_WillEnterFullScreen(ptr unsafe.Pointer, notification NSNotification) {
	if del := _WindowDelegate_load(ptr); del != nil && del.WillEnterFullScreen != nil {
		del.WillEnterFullScreen(notification)
	}
}

//export _WindowDelegate_DidEnterFullScreen
func _WindowDelegate_DidEnterFullScreen(ptr unsafe.Pointer, notification NSNotification) {
	if del := _WindowDelegate_load(ptr); del != nil && del.DidEnterFullScreen != nil {
		del.DidEnterFullScreen(notification)
	}
}

//export _WindowDelegate_WillExitFullScreen
func _WindowDelegate_WillExitFullScreen(ptr unsafe.Pointer, notification NSNotification) {
	if del := _WindowDelegate_load(ptr); del != nil && del.WillExitFullScreen != nil {
		del.WillExitFullScreen(notification)
	}
}

//export _WindowDelegate_DidExitFullScreen
func _WindowDelegate_DidExitFullScreen(ptr unsafe.Pointer, notification NSNotification) {
	if del := _WindowDelegate_load(ptr); del != nil && del.DidExitFullScreen != nil {
		del.DidExitFullScreen(notification)
	}
}

//export _WindowDelegate_WillEnterVersionBrowser
func _WindowDelegate_WillEnterVersionBrowser(ptr unsafe.Pointer, notification NSNotification) {
	if del := _WindowDelegate_load(ptr); del != nil && del.WillEnterVersionBrowser != nil {
		del.WillEnterVersionBrowser(notification)
	}
}

//export _WindowDelegate_DidEnterVersionBrowser
func _WindowDelegate_DidEnterVersionBrowser(ptr unsafe.Pointer, notification NSNotification) {
	if del := _WindowDelegate_load(ptr); del != nil && del.DidEnterVersionBrowser != nil {
		del.DidEnterVersionBrowser(notification)
	}
}

//export _WindowDelegate_WillExitVersionBrowser
func _WindowDelegate_WillExitVersionBrowser(ptr unsafe.Pointer, notification NSNotification) {
	if del := _WindowDelegate_load(ptr); del != nil && del.WillExitVersionBrowser != nil {
		del.WillExitVersionBrowser(notification)
	}
}

//export _WindowDelegate_DidExitVersionBrowser
func _WindowDelegate_DidExitVersionBrowser(ptr unsafe.Pointer, notification NSNotification) {
	if del := _WindowDelegate_load(ptr); del != nil && del.DidExitVersionBrowser != nil {
		del.DidExitVersionBrowser(notification)
	}
}

//export _WindowDelegate_DidChangeOcclusionState
func _WindowDelegate_DidChangeOcclusionState(ptr unsafe.Pointer, notification NSNotification) {
	if del := _WindowDelegate_load(ptr); del != nil && del.DidChangeOcclusionState != nil {
		del.DidChangeOcclusionState(notification)
	}
}
