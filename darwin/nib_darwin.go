/*
	Copyright (c) 2019 The Go App Authors

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

package darwin

/*
#import <AppKit/AppKit.h>

static void * loadData(void * bundle, void * data) {
	NSNib * nib = [[NSNib alloc] initWithNibData:data bundle:bundle];
	[(id)data release];
	return nib;
}

static void * loadNamed(void * bundle, void * name) {
	NSNib * nib = [[NSNib alloc] initWithNibNamed:name bundle:bundle];
	[(id)name release];
	return nib;
}

static bool instantiate(void * id, void * owner, void ** tlo) {
	if (!owner) {
		owner = NSApp;
	}
	return [(NSNib *)id instantiateWithOwner:owner topLevelObjects:(NSArray * _Nullable *)tlo];
}

static NSUInteger count(void * id) {
	return ((NSArray *)id).count;
}

static void * get(void * id, NSUInteger index) {
	return ((NSArray *)id)[index];
}
*/
import "C"

import (
	"runtime"
	"unsafe"

	"gitlab.com/firelizzard/go-app/cgo"
	"gitlab.com/firelizzard/go-app/objc"
)

type Nib struct {
	id  unsafe.Pointer
	arr unsafe.Pointer
}

// LoadNibFromData loads the nib from the data and instantiates it with owner.
// Returns nil on failure.
//
// If bundle is nil, the main bundle is used.
func LoadNibFromData(bundle unsafe.Pointer, data []byte) *Nib {
	nib := C.loadData(bundle, objc.NSData(data))
	if nib == nil {
		return nil
	}

	return &Nib{id: nib}
}

// LoadNibFromFile loads the nib from the file and instantiates it with owner.
// Returns nil on failure.
//
// If bundle is nil, the main bundle is used.
func LoadNibFromFile(bundle unsafe.Pointer, file string) *Nib {
	nib := C.loadData(bundle, objc.NSDataFromFile(file))
	if nib == nil {
		return nil
	}

	return &Nib{id: nib}
}

// LoadNibFromBundle loads the named nib from the bundle and instantiates it
// with owner. Returns nil on failure.
//
// If bundle is nil, the main bundle is used.
func LoadNibFromBundle(bundle unsafe.Pointer, name string) *Nib {
	nib := C.loadNamed(bundle, objc.NSString(name))
	if nib == nil {
		return nil
	}

	return &Nib{id: nib}
}

// Ok returns true if the nib is non-nil.
func (n *Nib) Ok() bool {
	return n != nil && n.id != nil
}

// Release releases the nib if it's valid (ok).
func (n *Nib) Release() {
	if n == nil || n.id == nil {
		return
	}
	objc.Release(n.id)
	// if n.arr != nil {
	// 	Release(n.arr)
	// }
	n.id = nil
	n.arr = nil
}

// Autorelease sets the finalizer for the nib to Release.
func (n *Nib) Autorelease() *Nib {
	if n == nil || n.id == nil {
		return nil
	}
	runtime.SetFinalizer(n, (*Nib).Release)
	return n
}

// Instantiate instantiates the nib resources with the owner.
//
// If the owner is nil, NSApp is used.
func (n *Nib) Instantiate(owner unsafe.Pointer) bool {
	if n == nil || n.id == nil {
		return false
	}
	if n.arr != nil {
		return true
	}

	return bool(C.instantiate(n.id, owner, &n.arr))
}

// InstantiateAndRelease calls Instantiate, fn, and Release.
func (n *Nib) InstantiateAndRelease(owner unsafe.Pointer, fn func(*Nib)) bool {
	if n == nil || n.id == nil {
		return false
	}
	if n.arr != nil {
		return true
	}

	defer n.Release()

	if !C.instantiate(n.id, owner, &n.arr) {
		return false
	}

	fn(n)
	return true
}

// Len returns the number of resources in the nib.
//
// Len will panic if the nib is not valid or has not been instantiated.
func (n *Nib) Len() uint {
	if n == nil || n.id == nil {
		panic("not ok")
	}
	if n.arr == nil {
		panic("not instantiated")
	}
	return uint(C.count(n.arr))
}

// Get returns the i'th resource of the nib.
//
// Nib resources do not load in a predictable order.
//
// Get will panic if the nib is not valid or has not been instantiated.
func (n *Nib) Get(i uint) unsafe.Pointer {
	if n == nil || n.id == nil {
		panic("not ok")
	}
	if n.arr == nil {
		panic("not instantiated")
	}
	return C.get(n.arr, C.NSUInteger(i))
}

// Each calls fn for each nib resource, stopping and returning the last object
// if fn returns true, or returning nil if fn always returns false.
//
// Each will panic if the nib is not valid or has not been instantiated.
func (n *Nib) Each(fn func(unsafe.Pointer) bool) unsafe.Pointer {
	if n == nil || n.id == nil {
		panic("not ok")
	}
	if n.arr == nil {
		panic("not instantiated")
	}

	N := n.Len()
	for i := uint(0); i < N; i++ {
		obj := n.Get(i)
		if fn(obj) {
			return obj
		}
	}
	return nil
}

// Find returns the first resource for which all functions return true.
//
// Find will panic if the nib is not valid or has not been instantiated.
func (n *Nib) Find(fns ...func(unsafe.Pointer) bool) unsafe.Pointer {
	if n == nil || n.id == nil {
		panic("not ok")
	}
	if n.arr == nil {
		panic("not instantiated")
	}

	filter := func(obj unsafe.Pointer) bool {
		for _, fn := range fns {
			if !fn(obj) {
				return false
			}
		}
		return true
	}

	N := n.Len()
	for i := uint(0); i < N; i++ {
		obj := n.Get(i)
		if filter(obj) {
			return obj
		}
	}
	return nil
}

// FindWindow finds a window from the nib and calls WindowFrom.
func (n *Nib) FindWindow(fns ...func(unsafe.Pointer) bool) (*Window, bool) {
	fns = append(fns, objc.FilterClass("NSWindow"))
	id := n.Find(fns...)
	if id == nil {
		return nil, false
	}
	return WindowFrom(id), true
}

// FindMenu finds a menu from the nib.
func (n *Nib) FindMenu(scope *cgo.Scope, fns ...func(unsafe.Pointer) bool) (*Menu, bool) {
	fns = append(fns, objc.FilterClass("NSMenu"))
	id := n.Find(fns...)
	if id == nil {
		return nil, false
	}
	return MenuFrom(id, scope), true
}
