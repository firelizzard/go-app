/*
	Copyright (c) 2019 The Go App Authors

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#import "menu_darwin.h"

void assertMainThread();
void callMenuActionHandler(void *, void *);

// TODO remove stringWithUTF8String because it uses autorelease

static inline SEL getsel(int i) {
	if (i == SEL_hide_)
		return @selector(hide:);
	if (i == SEL_hideOtherApplications_)
		return @selector(hideOtherApplications:);
	if (i == SEL_unhideAllApplications_)
		return @selector(unhideAllApplications:);
	if (i == SEL_terminate_)
		return @selector(terminate:);
	if (i == SEL_orderFrontStandardAboutPanel_)
		return @selector(orderFrontStandardAboutPanel:);
	return NULL;
}

@interface Function : NSObject
@property (readonly) void * handler;
@end

@implementation Function
- (id) init:(void *)handler
{
    if (self = [super init])
        _handler = handler;
    return self;
}

- (void) call:(id)sender
{
	callMenuActionHandler(_handler, sender);
}
@end

void * menuAddSubmenu(void * self, const char * title) {
	assertMainThread();

	NSString * _title = [NSString stringWithUTF8String:title]; free((void *)title);
	NSMenuItem * item = [[[NSMenuItem alloc] initWithTitle:_title action:NULL keyEquivalent:@""] autorelease];
	NSMenu * menu = [[[NSMenu alloc] initWithTitle:_title] autorelease];

	item.submenu = menu;
	[(NSMenu *)self addItem:item];
	return menu;
}

void menuAddSeparator(void * self) {
	assertMainThread();

	[(NSMenu *)self addItem:NSMenuItem.separatorItem];
}

void menuAddItem(void * self, const char * title, int action, const char * key, NSEventModifierFlags modifiers) {
	assertMainThread();

	NSMenuItem * item = [[NSMenuItem new] autorelease];
	[(NSMenu *)self addItem:item];

	item.action = getsel(action);
	item.title = [NSString stringWithUTF8String:title]; free((void *)title);
	item.keyEquivalent = [NSString stringWithUTF8String:key]; free((void *)key);
	if (modifiers) item.keyEquivalentModifierMask = modifiers;
}

void menuAddAction(void * self, const char * title, void * handler, const char * key, NSEventModifierFlags modifiers) {
	assertMainThread();

	NSMenuItem * item = [[NSMenuItem new] autorelease];
	[(NSMenu *)self addItem:item];

	item.target = [[[Function alloc] init:handler] autorelease];
	item.action = @selector(call:);
	item.title = [NSString stringWithUTF8String:title]; free((void *)title);
	item.keyEquivalent = [NSString stringWithUTF8String:key]; free((void *)key);
	if (modifiers) item.keyEquivalentModifierMask = modifiers;
}

void * menuItemAction(void * self) {
	assertMainThread();

	NSMenuItem * item = self;
	if (![item.target isKindOfClass:Function.class])
		return nil;

	return ((Function *)item.target).handler;
}

void menuItemSetAction(void * self, void * handler) {
	assertMainThread();

	NSMenuItem * item = self;
	item.target = [[[Function alloc] init:handler] autorelease];
	item.action = @selector(call:);
}