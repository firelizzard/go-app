/*
	Copyright (c) 2019 The Go App Authors

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

package darwin

/*
#import <AppKit/AppKit.h>

static void * newWindow(NSRect bounds, NSWindowStyleMask style) {
	return [[NSWindow alloc] initWithContentRect:bounds styleMask:style backing:NSBackingStoreBuffered defer:NO];
}

static void center(void * window) {
	[(NSWindow *)window center];
}

static void focus(void * window) {
	[(NSWindow *)window makeKeyAndOrderFront:NSApp];
}

static void wclose(void * window) {
	[(NSWindow *)window close];
}

static void * openPanel() {
	return [NSOpenPanel openPanel];
}

static void * savePanel() {
	return [NSSavePanel savePanel];
}

static void * newAlert() {
	return [NSAlert new];
}

static NSModalResponse showPanel(void * window, void * panel) {
	[(NSSavePanel *)panel beginSheetModalForWindow:(NSWindow *)window completionHandler:^(NSModalResponse result) {
		[NSApplication.sharedApplication stopModalWithCode:result];
	}];

	return [NSApplication.sharedApplication runModalForWindow:panel];
}

static void showAlert(void * alert) {
	[(NSAlert *)alert addButtonWithTitle:@"OK"];
	[(NSAlert *)alert runModal];
	[(NSAlert *)alert release];
}
*/
import "C"

import (
	"runtime"
	"unsafe"

	"gitlab.com/firelizzard/go-app/cgo"
	"gitlab.com/firelizzard/go-app/objc"
)

//go:generate go run ./gen/bind

const (
	defaultWindowDelegateShouldClose = true
)

type WindowSettings struct {
	Title        string
	Closeable    bool
	Minimizeable bool
	Resizeable   bool
	Width        int
	Height       int
	ContentView  unsafe.Pointer
}

type Window struct {
	id      unsafe.Pointer
	objcDel unsafe.Pointer
	goDel   cgo.Reference
}

// WindowFrom returns a Window for the given NSWindow.
//
// WindowFrom will panic if id is not an instance of NSWindow or a subclass.
func WindowFrom(id unsafe.Pointer) *Window {
	objc.AssertIsClass(id, "NSWindow")

	w := &Window{id: id}
	runtime.SetFinalizer(w, (*Window).finalize)
	return w
}

// NewWindow creates a window with the specified settings.
//
// NewWindow will panic if ContentView is non-nil but is not an instance of
// NSView or a subclass.
func NewWindow(s *WindowSettings) *Window {
	bounds := C.NSMakeRect(0, 0, C.double(s.Width), C.double(s.Height))

	style := C.NSWindowStyleMask(0)
	if s.Title != "" {
		style |= C.NSWindowStyleMaskTitled
	}
	if s.Closeable {
		style |= C.NSWindowStyleMaskClosable
	}
	if s.Minimizeable {
		style |= C.NSWindowStyleMaskMiniaturizable
	}
	if s.Resizeable {
		style |= C.NSWindowStyleMaskResizable
	}

	w := WindowFrom(C.newWindow(bounds, style))

	if s.Title != "" {
		w.SetTitle(s.Title)
	}

	if s.ContentView != nil {
		w.SetContentView(s.ContentView)
	}

	return w
}

func (w *Window) finalize() {
	w.SetDelegate(nil)
	objc.Release(w.id)
}

func (w *Window) Handle() unsafe.Pointer {
	return w.id
}

func (w *Window) Center() { C.center(w.id) }
func (w *Window) Focus()  { C.focus(w.id) }
func (w *Window) Close()  { C.wclose(w.id) }

func (w *Window) SetContentView(view unsafe.Pointer) {
	objc.AssertIsClass(view, "NSView")
	objc.SetValueForKey(w.id, view, "contentView")
}

func (w *Window) SetDelegate(del *WindowDelegate) {
	if w.objcDel != nil {
		objc.Release(w.objcDel)
		w.objcDel = nil
		w.goDel.Delete()
	}
	if del == nil {
		return
	}

	w.goDel, w.objcDel = del.AllocAndSave(nil)
	objc.SetValueForKey(w.id, w.objcDel, "delegate")
}

func (w *Window) SetTitle(title string) {
	t := objc.NSString(title)
	defer objc.Release(t)

	objc.SetValueForKey(w.id, t, "title")
}

func (w *Window) OpenFile() (string, bool) {
	panel := C.openPanel()

	objc.SetValueForKey(panel, objc.NSNumber(true), "canChooseFiles")
	objc.SetValueForKey(panel, objc.NSNumber(false), "canChooseDirectories")
	objc.SetValueForKey(panel, objc.NSNumber(false), "resolvesAliases")
	objc.SetValueForKey(panel, objc.NSNumber(false), "allowsMultipleSelection")

	objc.SetValueForKey(panel, objc.NSNumber(true), "canCreateDirectories")
	objc.SetValueForKey(panel, objc.NSNumber(true), "showsHiddenFiles")
	objc.SetValueForKey(panel, objc.NSNumber(false), "extensionHidden")
	objc.SetValueForKey(panel, objc.NSNumber(false), "canSelectHiddenExtension")
	objc.SetValueForKey(panel, objc.NSNumber(true), "treatsFilePackagesAsDirectories")

	if C.showPanel(w.id, panel) != C.NSModalResponseOK {
		return "", false
	}

	return objc.GoString(objc.ValueForKey(panel, "URL.path")), true
}

func (w *Window) SaveFile() (string, bool) {
	panel := C.savePanel()

	objc.SetValueForKey(panel, objc.NSNumber(true), "canCreateDirectories")
	objc.SetValueForKey(panel, objc.NSNumber(true), "showsHiddenFiles")
	objc.SetValueForKey(panel, objc.NSNumber(false), "extensionHidden")
	objc.SetValueForKey(panel, objc.NSNumber(false), "canSelectHiddenExtension")
	objc.SetValueForKey(panel, objc.NSNumber(true), "treatsFilePackagesAsDirectories")

	if C.showPanel(w.id, panel) != C.NSModalResponseOK {
		return "", false
	}

	return objc.GoString(objc.ValueForKey(panel, "URL.path")), true
}

type AlertType int

const (
	InfoAlert AlertType = iota
	WarningAlert
	ErrorAlert
)

func (w *Window) Alert(typ AlertType, title, message string) {
	alert := C.newAlert()
	defer objc.Release(alert)

	switch typ {
	case InfoAlert:
		objc.SetValueForKey(alert, objc.NSNumber(uint32(C.NSAlertStyleInformational)), "alertStyle")
	case WarningAlert:
		objc.SetValueForKey(alert, objc.NSNumber(uint32(C.NSAlertStyleWarning)), "alertStyle")
	case ErrorAlert:
		objc.SetValueForKey(alert, objc.NSNumber(uint32(C.NSAlertStyleCritical)), "alertStyle")
	}

	t := objc.NSString(title)
	defer objc.Release(t)
	m := objc.NSString(message)
	defer objc.Release(m)

	objc.SetValueForKey(alert, objc.NSNumber(false), "showsHelp")
	objc.SetValueForKey(alert, objc.NSNumber(false), "showsSuppressionButton")
	objc.SetValueForKey(alert, t, "messageText")
	objc.SetValueForKey(alert, m, "informativeText")
}
