/*
Copyright (c) 2019 The Go App Authors

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// THIS FILE IS GENERATED
// DO NOT MODIFY

#import <AppKit/AppKit.h>

BOOL _WindowDelegate_ShouldClose(void *, NSWindow *);
void _WindowDelegate_DidFailToEnterFullScreen(void *, NSWindow *);
void _WindowDelegate_DidFailToExitFullScreen(void *, NSWindow *);
void _WindowDelegate_WillEncodeRestorableState(void *, NSWindow *, NSCoder *);
void _WindowDelegate_DidDecodeRestorableState(void *, NSWindow *, NSCoder *);
void _WindowDelegate_DidResize(void *, NSNotification *);
void _WindowDelegate_DidExpose(void *, NSNotification *);
void _WindowDelegate_WillMove(void *, NSNotification *);
void _WindowDelegate_DidMove(void *, NSNotification *);
void _WindowDelegate_DidBecomeKey(void *, NSNotification *);
void _WindowDelegate_DidResignKey(void *, NSNotification *);
void _WindowDelegate_DidBecomeMain(void *, NSNotification *);
void _WindowDelegate_DidResignMain(void *, NSNotification *);
void _WindowDelegate_WillClose(void *, NSNotification *);
void _WindowDelegate_WillMiniaturize(void *, NSNotification *);
void _WindowDelegate_DidMiniaturize(void *, NSNotification *);
void _WindowDelegate_DidDeminiaturize(void *, NSNotification *);
void _WindowDelegate_DidUpdate(void *, NSNotification *);
void _WindowDelegate_DidChangeScreen(void *, NSNotification *);
void _WindowDelegate_DidChangeScreenProfile(void *, NSNotification *);
void _WindowDelegate_DidChangeBackingProperties(void *, NSNotification *);
void _WindowDelegate_WillBeginSheet(void *, NSNotification *);
void _WindowDelegate_DidEndSheet(void *, NSNotification *);
void _WindowDelegate_WillStartLiveResize(void *, NSNotification *);
void _WindowDelegate_DidEndLiveResize(void *, NSNotification *);
void _WindowDelegate_WillEnterFullScreen(void *, NSNotification *);
void _WindowDelegate_DidEnterFullScreen(void *, NSNotification *);
void _WindowDelegate_WillExitFullScreen(void *, NSNotification *);
void _WindowDelegate_DidExitFullScreen(void *, NSNotification *);
void _WindowDelegate_WillEnterVersionBrowser(void *, NSNotification *);
void _WindowDelegate_DidEnterVersionBrowser(void *, NSNotification *);
void _WindowDelegate_WillExitVersionBrowser(void *, NSNotification *);
void _WindowDelegate_DidExitVersionBrowser(void *, NSNotification *);
void _WindowDelegate_DidChangeOcclusionState(void *, NSNotification *);


@interface GoWindowDelegate : NSObject<NSWindowDelegate>
{ void * _go; }
@end

@implementation GoWindowDelegate
- (id) init:(void *)go
{
    if (!(self = [super init]))
		return nil;

	_go = go;
	return self;
}


- (BOOL) windowShouldClose:(NSWindow *)sender 
{
	return _WindowDelegate_ShouldClose(_go, sender);
}

- (void) windowDidFailToEnterFullScreen:(NSWindow *)window 
{
	_WindowDelegate_DidFailToEnterFullScreen(_go, window);
}

- (void) windowDidFailToExitFullScreen:(NSWindow *)window 
{
	_WindowDelegate_DidFailToExitFullScreen(_go, window);
}

- (void) window:(NSWindow *)window willEncodeRestorableState:(NSCoder *)state 
{
	_WindowDelegate_WillEncodeRestorableState(_go, window, state);
}

- (void) window:(NSWindow *)window didDecodeRestorableState:(NSCoder *)state 
{
	_WindowDelegate_DidDecodeRestorableState(_go, window, state);
}

- (void) windowDidResize:(NSNotification *)notification 
{
	_WindowDelegate_DidResize(_go, notification);
}

- (void) windowDidExpose:(NSNotification *)notification 
{
	_WindowDelegate_DidExpose(_go, notification);
}

- (void) windowWillMove:(NSNotification *)notification 
{
	_WindowDelegate_WillMove(_go, notification);
}

- (void) windowDidMove:(NSNotification *)notification 
{
	_WindowDelegate_DidMove(_go, notification);
}

- (void) windowDidBecomeKey:(NSNotification *)notification 
{
	_WindowDelegate_DidBecomeKey(_go, notification);
}

- (void) windowDidResignKey:(NSNotification *)notification 
{
	_WindowDelegate_DidResignKey(_go, notification);
}

- (void) windowDidBecomeMain:(NSNotification *)notification 
{
	_WindowDelegate_DidBecomeMain(_go, notification);
}

- (void) windowDidResignMain:(NSNotification *)notification 
{
	_WindowDelegate_DidResignMain(_go, notification);
}

- (void) windowWillClose:(NSNotification *)notification 
{
	_WindowDelegate_WillClose(_go, notification);
}

- (void) windowWillMiniaturize:(NSNotification *)notification 
{
	_WindowDelegate_WillMiniaturize(_go, notification);
}

- (void) windowDidMiniaturize:(NSNotification *)notification 
{
	_WindowDelegate_DidMiniaturize(_go, notification);
}

- (void) windowDidDeminiaturize:(NSNotification *)notification 
{
	_WindowDelegate_DidDeminiaturize(_go, notification);
}

- (void) windowDidUpdate:(NSNotification *)notification 
{
	_WindowDelegate_DidUpdate(_go, notification);
}

- (void) windowDidChangeScreen:(NSNotification *)notification 
{
	_WindowDelegate_DidChangeScreen(_go, notification);
}

- (void) windowDidChangeScreenProfile:(NSNotification *)notification 
{
	_WindowDelegate_DidChangeScreenProfile(_go, notification);
}

- (void) windowDidChangeBackingProperties:(NSNotification *)notification 
{
	_WindowDelegate_DidChangeBackingProperties(_go, notification);
}

- (void) windowWillBeginSheet:(NSNotification *)notification 
{
	_WindowDelegate_WillBeginSheet(_go, notification);
}

- (void) windowDidEndSheet:(NSNotification *)notification 
{
	_WindowDelegate_DidEndSheet(_go, notification);
}

- (void) windowWillStartLiveResize:(NSNotification *)notification 
{
	_WindowDelegate_WillStartLiveResize(_go, notification);
}

- (void) windowDidEndLiveResize:(NSNotification *)notification 
{
	_WindowDelegate_DidEndLiveResize(_go, notification);
}

- (void) windowWillEnterFullScreen:(NSNotification *)notification 
{
	_WindowDelegate_WillEnterFullScreen(_go, notification);
}

- (void) windowDidEnterFullScreen:(NSNotification *)notification 
{
	_WindowDelegate_DidEnterFullScreen(_go, notification);
}

- (void) windowWillExitFullScreen:(NSNotification *)notification 
{
	_WindowDelegate_WillExitFullScreen(_go, notification);
}

- (void) windowDidExitFullScreen:(NSNotification *)notification 
{
	_WindowDelegate_DidExitFullScreen(_go, notification);
}

- (void) windowWillEnterVersionBrowser:(NSNotification *)notification 
{
	_WindowDelegate_WillEnterVersionBrowser(_go, notification);
}

- (void) windowDidEnterVersionBrowser:(NSNotification *)notification 
{
	_WindowDelegate_DidEnterVersionBrowser(_go, notification);
}

- (void) windowWillExitVersionBrowser:(NSNotification *)notification 
{
	_WindowDelegate_WillExitVersionBrowser(_go, notification);
}

- (void) windowDidExitVersionBrowser:(NSNotification *)notification 
{
	_WindowDelegate_DidExitVersionBrowser(_go, notification);
}

- (void) windowDidChangeOcclusionState:(NSNotification *)notification 
{
	_WindowDelegate_DidChangeOcclusionState(_go, notification);
}
@end

void * newGoWindowDelegate(void * go) {
	return [[GoWindowDelegate alloc] init:go];
}
