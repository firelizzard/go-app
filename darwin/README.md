# Unsupported delegate methods

```objc
@protocol NSApplicationDelegate
- (void) application:(NSApplication *)application openURLs:(NSArray<NSURL *> *)urls
- (BOOL) application:(NSApplication *)sender openFile:(NSString *)filename
- (void) application:(NSApplication *)sender openFiles:(NSArray<NSString *> *)filenames
- (BOOL) application:(NSApplication *)sender openTempFile:(NSString *)filename
- (BOOL) applicationShouldOpenUntitledFile:(NSApplication *)sender
- (BOOL) applicationOpenUntitledFile:(NSApplication *)sender
- (BOOL) application:(id)sender openFileWithoutUI:(NSString *)filename
- (BOOL) application:(NSApplication *)sender printFile:(NSString *)filename
- (NSApplicationPrintReply) application:(NSApplication *)application printFiles:(NSArray<NSString *> *)fileNames withSettings:(NSDictionary<NSPrintInfoAttributeKey,id> *)printSettings showPrintPanels:(BOOL)showPrintPanels
- (NSMenu *) applicationDockMenu:(NSApplication *)sender
- (NSError *) application:(NSApplication *)application willPresentError:(NSError *)error
- (void) application:(NSApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
- (void) application:(NSApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
- (void) application:(NSApplication *)application didReceiveRemoteNotification:(NSDictionary<NSString *,id> *)userInfo
- (BOOL) application:(NSApplication *)application willContinueUserActivityWithType:(NSString *)userActivityType
- (BOOL) application:(NSApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^  )(NSArray<id<NSUserActivityRestoring>> * _Nonnull ))restorationHandler
- (void) application:(NSApplication *)application didFailToContinueUserActivityWithType:(NSString *)userActivityType error:(NSError *)error
- (void) application:(NSApplication *)application didUpdateUserActivity:(NSUserActivity *)userActivity
- (void) application:(NSApplication *)application userDidAcceptCloudKitShareWithMetadata:(CKShareMetadata *)metadata
- (BOOL) application:(NSApplication *)sender delegateHandlesKey:(NSString *)key
@end

@protocol NSWindowDelegate
- (NSApplicationPresentationOptions) window:(NSWindow *)window willUseFullScreenPresentationOptions:(NSApplicationPresentationOptions)proposedOptions

- (NSSize) windowWillResize:(NSWindow *)sender toSize:(NSSize)frameSize
- (NSSize) window:(NSWindow *)window willUseFullScreenContentSize:(NSSize)proposedSize
- (NSSize) window:(NSWindow *)window willResizeForVersionBrowserWithMaxPreferredSize:(NSSize)maxPreferredFrameSize maxAllowedSize:(NSSize)maxAllowedFrameSize

- (NSRect) windowWillUseStandardFrame:(NSWindow *)window defaultFrame:(NSRect)newFrame
- (BOOL) windowShouldZoom:(NSWindow *)window toFrame:(NSRect)newFrame
- (NSRect) window:(NSWindow *)window willPositionSheet:(NSWindow *)sheet usingRect:(NSRect)rect

- (void) window:(NSWindow *)window startCustomAnimationToEnterFullScreenWithDuration:(NSTimeInterval)duration
- (void) window:(NSWindow *)window startCustomAnimationToExitFullScreenWithDuration:(NSTimeInterval)duration
- (void) window:(NSWindow *)window startCustomAnimationToEnterFullScreenOnScreen:(NSScreen *)screen withDuration:(NSTimeInterval)duration

- (NSArray<NSWindow *> *) customWindowsToEnterFullScreenForWindow:(NSWindow *)window
- (NSArray<NSWindow *> *) customWindowsToExitFullScreenForWindow:(NSWindow *)window
- (NSArray<NSWindow *> *) customWindowsToEnterFullScreenForWindow:(NSWindow *)window onScreen:(NSScreen *)screen

- (id) windowWillReturnFieldEditor:(NSWindow *)sender toObject:(id)client
- (NSUndoManager *) windowWillReturnUndoManager:(NSWindow *)window
- (BOOL) window:(NSWindow *)window shouldPopUpDocumentPathMenu:(NSMenu *)menu
- (BOOL) window:(NSWindow *)window shouldDragDocumentWithEvent:(NSEvent *)event from:(NSPoint)dragImageLocation withPasteboard:(NSPasteboard *)pasteboard
@end
```