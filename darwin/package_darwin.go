/*
	Copyright (c) 2019 The Go App Authors

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

package darwin

/*
#cgo CFLAGS: -x objective-c
#cgo LDFLAGS: -framework Cocoa
#import <AppKit/AppKit.h>
*/
import "C"

import "unsafe"

type (
	NSApplication  = unsafe.Pointer
	NSCoder        = unsafe.Pointer
	NSNotification = unsafe.Pointer
	NSWindow       = unsafe.Pointer

	NSApplicationTerminateReply = C.NSApplicationTerminateReply
)

const (
	TerminateNow = C.NSTerminateNow
	TerminateCancel = C.NSTerminateCancel
	TerminateLater = C.NSTerminateLater
)