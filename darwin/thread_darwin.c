/*
	Copyright (c) 2019 The Go App Authors

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <pthread.h>

void GoPanic(const char * msg);

static uint64_t mainThreadID = -1;

static uint64_t threadID() {
	uint64_t id;
	if (pthread_threadid_np(pthread_self(), &id)) {
		abort();
	}
	return id;
}

void initMainThreadID() {
    mainThreadID = threadID();
}

void assertMainThread() {
    if (threadID() == mainThreadID)
        return;

    if (mainThreadID == -1)
        GoPanic("Main thread ID not initialized");
	else
		GoPanic("Invalid call - not on main thread");
}

void assertNotMainThread() {
    if (threadID() != mainThreadID)
        return;

    if (mainThreadID == -1)
        GoPanic("Main thread ID not initialized");
	else
		GoPanic("Invalid call - on main thread");
}