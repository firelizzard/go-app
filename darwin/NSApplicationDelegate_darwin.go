/*
Copyright (c) 2019 The Go App Authors

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// THIS FILE IS GENERATED
// DO NOT MODIFY

package darwin

// #import <AppKit/AppKit.h>
// void * newGoApplicationDelegate(void * go);
import "C" // DO NOT REMOVE

import (
	"unsafe"

	"gitlab.com/firelizzard/go-app/cgo"
)

func (x *ApplicationDelegate) AllocAndSave(s *cgo.Scope) (cgo.Reference, unsafe.Pointer) {
	r := cgo.Save(x)
	if s != nil {
		r.AttachTo(s)
	}

	id := C.newGoApplicationDelegate(r.C())
	return r, id
}

func (x *ApplicationDelegate) AllocAndSet(r cgo.Reference) unsafe.Pointer {
	r.Set(x)
	return C.newGoApplicationDelegate(r.C())
}

type ApplicationDelegate struct {
	ShouldTerminate func(sender NSApplication) NSApplicationTerminateReply
	ShouldTerminateAfterLastWindowClosed func(sender NSApplication) bool
	ShouldHandleReopenhasVisibleWindows func(sender NSApplication, flag bool) bool
	WillEncodeRestorableState func(app NSApplication, coder NSCoder)
	DidDecodeRestorableState func(app NSApplication, coder NSCoder)
	WillFinishLaunching func(notification NSNotification)
	DidFinishLaunching func(notification NSNotification)
	WillHide func(notification NSNotification)
	DidHide func(notification NSNotification)
	WillUnhide func(notification NSNotification)
	DidUnhide func(notification NSNotification)
	WillBecomeActive func(notification NSNotification)
	DidBecomeActive func(notification NSNotification)
	WillResignActive func(notification NSNotification)
	DidResignActive func(notification NSNotification)
	WillUpdate func(notification NSNotification)
	DidUpdate func(notification NSNotification)
	WillTerminate func(notification NSNotification)
	DidChangeScreenParameters func(notification NSNotification)
	DidChangeOcclusionState func(notification NSNotification)
}

func _ApplicationDelegate_load(ptr unsafe.Pointer) *ApplicationDelegate {
	if ptr == nil {
		return nil
	}

	v := cgo.Reference(ptr).Load()
	if v == nil {
		return nil
	}

	return v.(*ApplicationDelegate)
}

//export _ApplicationDelegate_ShouldTerminate
func _ApplicationDelegate_ShouldTerminate(ptr unsafe.Pointer, sender NSApplication) NSApplicationTerminateReply {
	if del := _ApplicationDelegate_load(ptr); del != nil && del.ShouldTerminate != nil {
		return del.ShouldTerminate(sender)
	}
	return defaultApplicationDelegateShouldTerminate
}

//export _ApplicationDelegate_ShouldTerminateAfterLastWindowClosed
func _ApplicationDelegate_ShouldTerminateAfterLastWindowClosed(ptr unsafe.Pointer, sender NSApplication) bool {
	if del := _ApplicationDelegate_load(ptr); del != nil && del.ShouldTerminateAfterLastWindowClosed != nil {
		return del.ShouldTerminateAfterLastWindowClosed(sender)
	}
	return defaultApplicationDelegateShouldTerminateAfterLastWindowClosed
}

//export _ApplicationDelegate_ShouldHandleReopenhasVisibleWindows
func _ApplicationDelegate_ShouldHandleReopenhasVisibleWindows(ptr unsafe.Pointer, sender NSApplication, flag bool) bool {
	if del := _ApplicationDelegate_load(ptr); del != nil && del.ShouldHandleReopenhasVisibleWindows != nil {
		return del.ShouldHandleReopenhasVisibleWindows(sender, flag)
	}
	return defaultApplicationDelegateShouldHandleReopenhasVisibleWindows
}

//export _ApplicationDelegate_WillEncodeRestorableState
func _ApplicationDelegate_WillEncodeRestorableState(ptr unsafe.Pointer, app NSApplication, coder NSCoder) {
	if del := _ApplicationDelegate_load(ptr); del != nil && del.WillEncodeRestorableState != nil {
		del.WillEncodeRestorableState(app, coder)
	}
}

//export _ApplicationDelegate_DidDecodeRestorableState
func _ApplicationDelegate_DidDecodeRestorableState(ptr unsafe.Pointer, app NSApplication, coder NSCoder) {
	if del := _ApplicationDelegate_load(ptr); del != nil && del.DidDecodeRestorableState != nil {
		del.DidDecodeRestorableState(app, coder)
	}
}

//export _ApplicationDelegate_WillFinishLaunching
func _ApplicationDelegate_WillFinishLaunching(ptr unsafe.Pointer, notification NSNotification) {
	if del := _ApplicationDelegate_load(ptr); del != nil && del.WillFinishLaunching != nil {
		del.WillFinishLaunching(notification)
	}
}

//export _ApplicationDelegate_DidFinishLaunching
func _ApplicationDelegate_DidFinishLaunching(ptr unsafe.Pointer, notification NSNotification) {
	if del := _ApplicationDelegate_load(ptr); del != nil && del.DidFinishLaunching != nil {
		del.DidFinishLaunching(notification)
	}
}

//export _ApplicationDelegate_WillHide
func _ApplicationDelegate_WillHide(ptr unsafe.Pointer, notification NSNotification) {
	if del := _ApplicationDelegate_load(ptr); del != nil && del.WillHide != nil {
		del.WillHide(notification)
	}
}

//export _ApplicationDelegate_DidHide
func _ApplicationDelegate_DidHide(ptr unsafe.Pointer, notification NSNotification) {
	if del := _ApplicationDelegate_load(ptr); del != nil && del.DidHide != nil {
		del.DidHide(notification)
	}
}

//export _ApplicationDelegate_WillUnhide
func _ApplicationDelegate_WillUnhide(ptr unsafe.Pointer, notification NSNotification) {
	if del := _ApplicationDelegate_load(ptr); del != nil && del.WillUnhide != nil {
		del.WillUnhide(notification)
	}
}

//export _ApplicationDelegate_DidUnhide
func _ApplicationDelegate_DidUnhide(ptr unsafe.Pointer, notification NSNotification) {
	if del := _ApplicationDelegate_load(ptr); del != nil && del.DidUnhide != nil {
		del.DidUnhide(notification)
	}
}

//export _ApplicationDelegate_WillBecomeActive
func _ApplicationDelegate_WillBecomeActive(ptr unsafe.Pointer, notification NSNotification) {
	if del := _ApplicationDelegate_load(ptr); del != nil && del.WillBecomeActive != nil {
		del.WillBecomeActive(notification)
	}
}

//export _ApplicationDelegate_DidBecomeActive
func _ApplicationDelegate_DidBecomeActive(ptr unsafe.Pointer, notification NSNotification) {
	if del := _ApplicationDelegate_load(ptr); del != nil && del.DidBecomeActive != nil {
		del.DidBecomeActive(notification)
	}
}

//export _ApplicationDelegate_WillResignActive
func _ApplicationDelegate_WillResignActive(ptr unsafe.Pointer, notification NSNotification) {
	if del := _ApplicationDelegate_load(ptr); del != nil && del.WillResignActive != nil {
		del.WillResignActive(notification)
	}
}

//export _ApplicationDelegate_DidResignActive
func _ApplicationDelegate_DidResignActive(ptr unsafe.Pointer, notification NSNotification) {
	if del := _ApplicationDelegate_load(ptr); del != nil && del.DidResignActive != nil {
		del.DidResignActive(notification)
	}
}

//export _ApplicationDelegate_WillUpdate
func _ApplicationDelegate_WillUpdate(ptr unsafe.Pointer, notification NSNotification) {
	if del := _ApplicationDelegate_load(ptr); del != nil && del.WillUpdate != nil {
		del.WillUpdate(notification)
	}
}

//export _ApplicationDelegate_DidUpdate
func _ApplicationDelegate_DidUpdate(ptr unsafe.Pointer, notification NSNotification) {
	if del := _ApplicationDelegate_load(ptr); del != nil && del.DidUpdate != nil {
		del.DidUpdate(notification)
	}
}

//export _ApplicationDelegate_WillTerminate
func _ApplicationDelegate_WillTerminate(ptr unsafe.Pointer, notification NSNotification) {
	if del := _ApplicationDelegate_load(ptr); del != nil && del.WillTerminate != nil {
		del.WillTerminate(notification)
	}
}

//export _ApplicationDelegate_DidChangeScreenParameters
func _ApplicationDelegate_DidChangeScreenParameters(ptr unsafe.Pointer, notification NSNotification) {
	if del := _ApplicationDelegate_load(ptr); del != nil && del.DidChangeScreenParameters != nil {
		del.DidChangeScreenParameters(notification)
	}
}

//export _ApplicationDelegate_DidChangeOcclusionState
func _ApplicationDelegate_DidChangeOcclusionState(ptr unsafe.Pointer, notification NSNotification) {
	if del := _ApplicationDelegate_load(ptr); del != nil && del.DidChangeOcclusionState != nil {
		del.DidChangeOcclusionState(notification)
	}
}
