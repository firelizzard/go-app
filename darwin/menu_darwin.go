/*
	Copyright (c) 2019 The Go App Authors

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

package darwin

/*
#import "menu_darwin.h"

void * menuAddSubmenu(void * self, const char * title);
void menuAddSeparator(void * self);
void menuAddItem(void * self, const char * title, int action, const char * key, NSEventModifierFlags modifiers);
void menuAddAction(void * self, const char * title, void * handler, const char * key, NSEventModifierFlags modifiers);
void * menuItemAction(void * self);
void menuItemSetAction(void * self, void * handler);

static void * menuItems(void * self) {
	return [(NSMenu *)self itemArray];
}

static const char * menuItemTitle(void * self) {
	return [(NSMenuItem *)self title].UTF8String;
}

static void * menuItemSubmenu(void * self) {
	return [(NSMenuItem *)self submenu];
}
*/
import "C"

import (
	"unsafe"

	"gitlab.com/firelizzard/go-app/cgo"
	"gitlab.com/firelizzard/go-app/objc"
)

type MenuAction int
type EventModifierFlag uint

var (
	HideAction           = MenuAction(C.SEL_hide_)
	HideOthersAction     = MenuAction(C.SEL_hideOtherApplications_)
	ShowAllAction        = MenuAction(C.SEL_unhideAllApplications_)
	TerminateAction      = MenuAction(C.SEL_terminate_)
	ShowAboutPanelAction = MenuAction(C.SEL_orderFrontStandardAboutPanel_)

	EventModifierFlagCapsLock                   = EventModifierFlag(C.NSEventModifierFlagCapsLock)
	EventModifierFlagShift                      = EventModifierFlag(C.NSEventModifierFlagShift)
	EventModifierFlagControl                    = EventModifierFlag(C.NSEventModifierFlagControl)
	EventModifierFlagOption                     = EventModifierFlag(C.NSEventModifierFlagOption)
	EventModifierFlagCommand                    = EventModifierFlag(C.NSEventModifierFlagCommand)
	EventModifierFlagNumericPad                 = EventModifierFlag(C.NSEventModifierFlagNumericPad)
	EventModifierFlagHelp                       = EventModifierFlag(C.NSEventModifierFlagHelp)
	EventModifierFlagFunction                   = EventModifierFlag(C.NSEventModifierFlagFunction)
	EventModifierFlagDeviceIndependentFlagsMask = EventModifierFlag(C.NSEventModifierFlagDeviceIndependentFlagsMask)
)

type Menu struct {
	id    unsafe.Pointer
	scope *cgo.Scope
}

type MenuItem struct {
	id    unsafe.Pointer
	scope *cgo.Scope
}

func MenuFrom(id unsafe.Pointer, scope *cgo.Scope) *Menu {
	if scope == nil {
		scope = new(cgo.Scope)
	}
	return &Menu{id, scope}
}

func MenuItemFrom(id unsafe.Pointer, scope *cgo.Scope) *MenuItem {
	if scope == nil {
		panic("scope is required")
	}
	return &MenuItem{id, scope}
}

func (m *Menu) Handle() unsafe.Pointer {
	return m.id
}

func (m *Menu) Scope() *cgo.Scope {
	return m.scope
}

func (m *Menu) Items() *objc.Array {
	return objc.ArrayFrom(C.menuItems(m.id))
}

func (m *Menu) AddSubmenu(title string) *Menu {
	sub := C.menuAddSubmenu(m.id, C.CString(title))
	return MenuFrom(sub, m.scope)
}

func (m *Menu) AddSeparator() {
	C.menuAddSeparator(m.id)
}

func (m *Menu) AddMenuItem(title string, action MenuAction, key string, modifiers EventModifierFlag) {
	C.menuAddItem(m.id, C.CString(title), C.int(action), C.CString(key), C.NSEventModifierFlags(modifiers))
}

func (m *Menu) AddMenuAction(title, key string, modifiers EventModifierFlag, fn func(unsafe.Pointer)) {
	C.menuAddAction(m.id, C.CString(title), m.scope.Save(fn).C(), C.CString(key), C.NSEventModifierFlags(modifiers))
}

func (m *Menu) GetItem(i uint) *MenuItem {
	return MenuItemFrom(m.Items().Get(i), m.scope)
}

func (i *MenuItem) Title() string {
	return C.GoString(C.menuItemTitle(i.id))
}

func (i *MenuItem) Submenu() *Menu {
	return MenuFrom(C.menuItemSubmenu(i.id), i.scope)
}

func (i *MenuItem) SetAction(fn func(unsafe.Pointer)) {
	old := C.menuItemAction(i.id)
	if old != nil {
		// this is safe to do even if the old value is not a reference
		cgo.Reference(old).Delete()
	}

	C.menuItemSetAction(i.id, i.scope.Save(fn).C())
}

//export callMenuActionHandler
func callMenuActionHandler(fn uintptr, sender unsafe.Pointer) {
	cgo.Reference(fn).Load().(func(unsafe.Pointer))(sender)
}
