/*
	Copyright (c) 2019 The Go App Authors

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

package main

import (
	"log"
	"os/exec"
	"strings"

	"gitlab.com/firelizzard/go-app/darwin/gen"
)

const copySrc = `/*
Copyright (c) 2019 The Go App Authors

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// THIS FILE IS GENERATED
// DO NOT MODIFY
`

func init() {
	gen.Init(copySrc)
}

func main() {
	sdkPathOut, err := exec.Command("xcrun", "--show-sdk-path").CombinedOutput()
	if err != nil {
		log.Fatal(err)
	}
	sdkPath := strings.TrimSpace(string(sdkPathOut))

	gen.Bind(sdkPath, "AppKit", "NSApplication", "NSApplicationDelegate", "application", []string{
		"application:openURLs:",
		"application:openFile:",
		"application:openFiles:",
		"application:openTempFile:",
		"applicationShouldOpenUntitledFile:",
		"applicationOpenUntitledFile:",
		"application:openFileWithoutUI:",
		"application:printFile:",
		"application:printFiles:withSettings:showPrintPanels:",
		"applicationDockMenu:",
		"application:willPresentError:",
		"application:didRegisterForRemoteNotificationsWithDeviceToken:",
		"application:didFailToRegisterForRemoteNotificationsWithError:",
		"application:didReceiveRemoteNotification:",
		"application:willContinueUserActivityWithType:",
		"application:continueUserActivity:restorationHandler:",
		"application:didFailToContinueUserActivityWithType:error:",
		"application:didUpdateUserActivity:",
		"application:userDidAcceptCloudKitShareWithMetadata:",
		"application:delegateHandlesKey:",
	})

	gen.Bind(sdkPath, "AppKit", "NSWindow", "NSWindowDelegate", "window", []string{
		"window:willUseFullScreenPresentationOptions:",
		"windowWillResize:toSize:",
		"window:willUseFullScreenContentSize:",
		"window:willResizeForVersionBrowserWithMaxPreferredSize:maxAllowedSize:",
		"windowWillUseStandardFrame:defaultFrame:",
		"windowShouldZoom:toFrame:",
		"window:willPositionSheet:usingRect:",
		"window:startCustomAnimationToEnterFullScreenWithDuration:",
		"window:startCustomAnimationToExitFullScreenWithDuration:",
		"window:startCustomAnimationToEnterFullScreenOnScreen:withDuration:",
		"customWindowsToEnterFullScreenForWindow:",
		"customWindowsToExitFullScreenForWindow:",
		"customWindowsToEnterFullScreenForWindow:onScreen:",
		"windowWillReturnFieldEditor:toObject:",
		"windowWillReturnUndoManager:",
		"window:shouldPopUpDocumentPathMenu:",
		"window:shouldDragDocumentWithEvent:from:withPasteboard:",
	})
}
