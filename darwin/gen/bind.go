/*
	Copyright (c) 2019 The Go App Authors

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

package gen

import (
	"bufio"
	"bytes"
	"fmt"
	"log"
	"os"
	"os/exec"
	"strings"
	"text/template"
)

const goSrc = `
package darwin

// #import <AppKit/AppKit.h>
// void * newGo{{ .Name }}(void * go);
import "C" // DO NOT REMOVE

import (
	"unsafe"

	"gitlab.com/firelizzard/go-app/cgo"
)

func (x *{{ .Name }}) AllocAndSave(s *cgo.Scope) (cgo.Reference, unsafe.Pointer) {
	r := cgo.Save(x)
	if s != nil {
		r.AttachTo(s)
	}

	id := C.newGo{{ .Name }}(r.C())
	return r, id
}

func (x *{{ .Name }}) AllocAndSet(r cgo.Reference) unsafe.Pointer {
	r.Set(x)
	return C.newGo{{ .Name }}(r.C())
}

type {{ .Name }} struct {
{{ range .Methods }}	{{ .GoName }} func({{ range $i, $p := .Params }}{{ if ne $i 0 }}, {{ end }}{{ $p.Name }} {{ $p.GoType }}{{ end }}){{ if .Return }} {{ .Return.GoType }}{{ end }}
{{ end }}}

func _{{ .Name }}_load(ptr unsafe.Pointer) *{{ .Name }} {
	if ptr == nil {
		return nil
	}

	v := cgo.Reference(ptr).Load()
	if v == nil {
		return nil
	}

	return v.(*{{ .Name }})
}
{{ $t := .Name }}{{ range .Methods }}
//export {{ .CName }}
func {{ .CName }}(ptr unsafe.Pointer{{ range .Params }}, {{ .Name }} {{ .GoType }}{{ end }}){{ if .Return }} {{ .Return.GoType }}{{ end }} {
	if del := _{{ $t }}_load(ptr); del != nil && del.{{ .GoName }} != nil {
		{{ if .Return }}return {{ end }}del.{{ .GoName }}({{ range $i, $p := .Params }}{{ if ne $i 0 }}, {{ end }}{{ $p.Name }}{{ end }})
	}{{ if .Return }}
	return default{{ $t }}{{ .GoName }}{{ end }}
}
{{end}}`

const objcSrc = `
#import <AppKit/AppKit.h>

{{ range .Methods }}{{ if .Return }}{{ .Return.CType }}{{ else }}void{{ end }} {{ .CName }}(void *{{ range .Params }}, {{ .CType }}{{ end }});
{{ end }}

@interface Go{{ .Name }} : NSObject<NS{{ .Name }}>
{ void * _go; }
@end

@implementation Go{{ .Name }}
- (id) init:(void *)go
{
    if (!(self = [super init]))
		return nil;

	_go = go;
	return self;
}

{{ range .Methods }}
- ({{ if .Return }}{{ .Return.CType }}{{ else }}void{{ end }}) {{ .Signature }}
{
	{{if .Return}}return {{end}}{{ .CName }}(_go{{ range .Params }}, {{ .Name }}{{ end }});
}
{{end}}@end

void * newGo{{ .Name }}(void * go) {
	return [[Go{{ .Name }} alloc] init:go];
}
`

var tmpls *template.Template

func Init(preamble string) {
	tmpls = template.New("")

	_, err := tmpls.New("go").Parse(preamble + goSrc)
	if err != nil {
		log.Fatal(err)
	}

	_, err = tmpls.New("objc").Parse(preamble + objcSrc)
	if err != nil {
		log.Fatal(err)
	}
}

func Bind(sdk, framework, file, typ, prefix string, skip []string) {
	type TypeBinding struct {
		CType  string
		GoType string
	}

	type ParamBinding struct {
		Name string
		TypeBinding
	}

	type MethodBinding struct {
		CName     string
		GoName    string
		Signature string
		Params    []*ParamBinding
		Return    *TypeBinding
	}

	type ProtocolBinding struct {
		Name    string
		Methods []*MethodBinding
	}

	hFile := sdk + frameworksPath + framework + ".framework/Versions/Current/Headers/" + file + ".h"
	out, err := exec.Command("xcrun", "clang", "-cc1", "-isysroot", sdk, "-ast-dump", "-fblocks", "-fobjc-arc", "-x", "objective-c", hFile).CombinedOutput()
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s", out)
		log.Fatal(err)
	}

	p := &parser{
		sdkPath: sdk,
		sc:      bufio.NewScanner(bytes.NewBuffer(out)),
	}

	typeMap := map[string]*Named{}
	if err := p.parseModule(framework, typeMap); err != nil {
		log.Fatal(err)
	}

	named, ok := typeMap[typ]
	if !ok {
		log.Fatalf("Could not find %s in %s", typ, file)
	}

	proto := new(ProtocolBinding)
	if strings.HasPrefix(typ, "NS") {
		proto.Name = typ[2:]
	} else {
		proto.Name = typ
	}

	skipMap := map[string]struct{}{}
	for _, s := range skip {
		skipMap[s] = struct{}{}
	}

	for _, m := range named.Methods {
		if _, ok := skipMap[m.Sig]; ok {
			delete(skipMap, m.Sig)
			continue
		}

		method := new(MethodBinding)
		proto.Methods = append(proto.Methods, method)

		method.GoName = strings.ReplaceAll(m.Sig, ":", "")
		if strings.HasPrefix(method.GoName, prefix) {
			method.GoName = method.GoName[len(prefix):]
		}
		method.GoName = strings.Title(method.GoName)
		method.CName = "_" + proto.Name + "_" + method.GoName

		sig := strings.Split(m.Sig, ":")
		method.Signature, sig = sig[0], sig[1:]

		if len(sig) != len(m.Params) {
			log.Fatal("Method signature does not match parameters")
		}
		for i, p := range m.Params {
			method.Signature += ":(" + p.Type.Decl + ")" + p.Name + " " + sig[i]

			param := new(ParamBinding)
			method.Params = append(method.Params, param)

			param.Name = p.Name
			param.CType = p.Type.Decl
			param.GoType = goTypeFor(p.Type)
		}

		if m.Ret == nil {
			continue
		}

		method.Return = new(TypeBinding)
		method.Return.CType = m.Ret.Decl
		method.Return.GoType = goTypeFor(m.Ret)
	}

	if len(skipMap) > 0 {
		skip = skip[:0]
		for s := range skipMap {
			skip = append(skip, s)
		}
		log.Fatalf("Unknown methods: %s", strings.Join(skip, ", "))
	}

	f, err := os.Create(typ + "_darwin.go")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	err = tmpls.ExecuteTemplate(f, "go", proto)
	if err != nil {
		log.Fatal(err)
	}

	f, err = os.Create(typ + "_darwin.m")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	err = tmpls.ExecuteTemplate(f, "objc", proto)
	if err != nil {
		log.Fatal(err)
	}
}

func goTypeFor(typ *Type) string {
	if typ.Kind == Bool || typ.Decl == "BOOL" {
		return "bool"
	}

	switch typ.Kind {
	case Protocol, Class:
		if typ.Name == "" {
			return "unsafe.Pointer"
		}
		return typ.Name

	case String:
		return "string"

	case Data:
		return "[]byte"

	default:
		return typ.Decl
	}
}
