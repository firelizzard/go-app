/*
	Copyright (c) 2019 The Go App Authors

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

package darwin

/*
#import <AppKit/AppKit.h>

void assertMainThread();

@interface GoApplicationDelegate : NSObject<NSApplicationDelegate>
- (id) init:(void *)go;
@end

static void appInit() {
	[NSAutoreleasePool new];
    [NSApplication sharedApplication];

	NSApp.activationPolicy = NSApplicationActivationPolicyRegular;
}

static void appSetDelegate(void * del) {
	NSApp.delegate = del;
}

static void appRun() {
    assertMainThread();
    [NSApp run];
}

static void appActivate() {
    [NSApp activateIgnoringOtherApps:YES];
}

static void appStop() {
    [NSApp stop:NSApp];
}

static void appTerminate() {
    [NSApp terminate:NSApp];
}

static const char * appProcessName() {
    return NSProcessInfo.processInfo.processName.UTF8String;
}

static void * appMainMenu() {
	if (NSApp.mainMenu)
    	return NSApp.mainMenu;
	return NSApp.mainMenu = [[NSMenu new] autorelease];
}
*/
import "C"

import (
	"unsafe"

	"gitlab.com/firelizzard/go-app/cgo"
	"gitlab.com/firelizzard/go-app/event"
	"gitlab.com/firelizzard/go-app/objc"
)

func init() {
	C.appInit()
	SetDelegate(nil)
}

const (
	defaultApplicationDelegateShouldTerminate                      = TerminateNow
	defaultApplicationDelegateShouldTerminateAfterLastWindowClosed = false
	defaultApplicationDelegateShouldHandleReopenhasVisibleWindows  = false
)

type Settings struct {
	AppName     string
	Menu        *Menu
	About       *Window
	Preferences *Window
}

func Start(s *Settings) {
	if s == nil {
		s = new(Settings)
	}

	if s.AppName == "" {
		s.AppName = ProcessName()
	}

	if s.Menu == nil {
		s.Menu = CreateAppMenu(s.AppName, s.About, s.Preferences)
	}
}

func Run()       { C.appRun() }
func Activate()  { C.appActivate() }
func Stop()      { C.appStop() }
func Terminate() { C.appTerminate() }

func MainMenu() *Menu {
	return MenuFrom(C.appMainMenu(), cgo.GlobalScope)
}

func ProcessName() string {
	return C.GoString(C.appProcessName())
}

func CreateAppMenu(name string, about, prefs *Window) *Menu {
	m := MainMenu().AddSubmenu(name)

	if about == nil {
		m.AddMenuItem("About "+name, ShowAboutPanelAction, "", 0)
	} else {
		m.AddMenuAction("About "+name, "", 0, func(unsafe.Pointer) {
			about.Center()
			about.Focus()
		})
	}

	m.AddSeparator()

	if prefs != nil {
		m.AddMenuAction("Preferences...", ",", 0, func(unsafe.Pointer) {
			prefs.Center()
			prefs.Focus()
		})

		m.AddSeparator()
	}

	m.AddMenuItem("Hide "+name, HideAction, "h", 0)
	m.AddMenuItem("Hide Others", HideOthersAction, "h", EventModifierFlagCommand|EventModifierFlagOption)
	m.AddMenuItem("Show All", ShowAllAction, "", 0)

	m.AddSeparator()

	m.AddMenuItem("Quit "+name, TerminateAction, "q", 0)

	return m
}

var appDelObjc unsafe.Pointer
var appDelGo = cgo.Save(nil)

func SetDelegate(del *ApplicationDelegate) {
	if appDelObjc != nil {
		objc.Release(appDelObjc)
		appDelObjc = nil
		appDelGo.Delete()
	}

	if del == nil {
		del = new(ApplicationDelegate)
	}

	didFinishLaunching := del.DidFinishLaunching
	del.DidFinishLaunching = func(n NSNotification) {
		event.Alive.Send()
		C.appActivate()
		if didFinishLaunching != nil {
			didFinishLaunching(n)
		}
		event.Visible.Send()
	}

	willTerminate := del.WillTerminate
	del.WillTerminate = func(n NSNotification) {
		event.Dead.Send()
		if willTerminate != nil {
			willTerminate(n)
		}
	}

	didHide := del.DidHide
	del.DidHide = func(n NSNotification) {
		event.Alive.Send()
		if didHide != nil {
			didHide(n)
		}
	}

	willUnhide := del.WillUnhide
	del.WillUnhide = func(n NSNotification) {
		event.Visible.Send()
		if willUnhide != nil {
			willUnhide(n)
		}
	}

	appDelObjc = del.AllocAndSet(appDelGo)
	C.appSetDelegate(appDelObjc)
}
