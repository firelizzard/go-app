/*
Copyright (c) 2019 The Go App Authors

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// THIS FILE IS GENERATED
// DO NOT MODIFY

#import <AppKit/AppKit.h>

NSApplicationTerminateReply _ApplicationDelegate_ShouldTerminate(void *, NSApplication *);
BOOL _ApplicationDelegate_ShouldTerminateAfterLastWindowClosed(void *, NSApplication *);
BOOL _ApplicationDelegate_ShouldHandleReopenhasVisibleWindows(void *, NSApplication *, BOOL);
void _ApplicationDelegate_WillEncodeRestorableState(void *, NSApplication *, NSCoder *);
void _ApplicationDelegate_DidDecodeRestorableState(void *, NSApplication *, NSCoder *);
void _ApplicationDelegate_WillFinishLaunching(void *, NSNotification *);
void _ApplicationDelegate_DidFinishLaunching(void *, NSNotification *);
void _ApplicationDelegate_WillHide(void *, NSNotification *);
void _ApplicationDelegate_DidHide(void *, NSNotification *);
void _ApplicationDelegate_WillUnhide(void *, NSNotification *);
void _ApplicationDelegate_DidUnhide(void *, NSNotification *);
void _ApplicationDelegate_WillBecomeActive(void *, NSNotification *);
void _ApplicationDelegate_DidBecomeActive(void *, NSNotification *);
void _ApplicationDelegate_WillResignActive(void *, NSNotification *);
void _ApplicationDelegate_DidResignActive(void *, NSNotification *);
void _ApplicationDelegate_WillUpdate(void *, NSNotification *);
void _ApplicationDelegate_DidUpdate(void *, NSNotification *);
void _ApplicationDelegate_WillTerminate(void *, NSNotification *);
void _ApplicationDelegate_DidChangeScreenParameters(void *, NSNotification *);
void _ApplicationDelegate_DidChangeOcclusionState(void *, NSNotification *);


@interface GoApplicationDelegate : NSObject<NSApplicationDelegate>
{ void * _go; }
@end

@implementation GoApplicationDelegate
- (id) init:(void *)go
{
    if (!(self = [super init]))
		return nil;

	_go = go;
	return self;
}


- (NSApplicationTerminateReply) applicationShouldTerminate:(NSApplication *)sender 
{
	return _ApplicationDelegate_ShouldTerminate(_go, sender);
}

- (BOOL) applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)sender 
{
	return _ApplicationDelegate_ShouldTerminateAfterLastWindowClosed(_go, sender);
}

- (BOOL) applicationShouldHandleReopen:(NSApplication *)sender hasVisibleWindows:(BOOL)flag 
{
	return _ApplicationDelegate_ShouldHandleReopenhasVisibleWindows(_go, sender, flag);
}

- (void) application:(NSApplication *)app willEncodeRestorableState:(NSCoder *)coder 
{
	_ApplicationDelegate_WillEncodeRestorableState(_go, app, coder);
}

- (void) application:(NSApplication *)app didDecodeRestorableState:(NSCoder *)coder 
{
	_ApplicationDelegate_DidDecodeRestorableState(_go, app, coder);
}

- (void) applicationWillFinishLaunching:(NSNotification *)notification 
{
	_ApplicationDelegate_WillFinishLaunching(_go, notification);
}

- (void) applicationDidFinishLaunching:(NSNotification *)notification 
{
	_ApplicationDelegate_DidFinishLaunching(_go, notification);
}

- (void) applicationWillHide:(NSNotification *)notification 
{
	_ApplicationDelegate_WillHide(_go, notification);
}

- (void) applicationDidHide:(NSNotification *)notification 
{
	_ApplicationDelegate_DidHide(_go, notification);
}

- (void) applicationWillUnhide:(NSNotification *)notification 
{
	_ApplicationDelegate_WillUnhide(_go, notification);
}

- (void) applicationDidUnhide:(NSNotification *)notification 
{
	_ApplicationDelegate_DidUnhide(_go, notification);
}

- (void) applicationWillBecomeActive:(NSNotification *)notification 
{
	_ApplicationDelegate_WillBecomeActive(_go, notification);
}

- (void) applicationDidBecomeActive:(NSNotification *)notification 
{
	_ApplicationDelegate_DidBecomeActive(_go, notification);
}

- (void) applicationWillResignActive:(NSNotification *)notification 
{
	_ApplicationDelegate_WillResignActive(_go, notification);
}

- (void) applicationDidResignActive:(NSNotification *)notification 
{
	_ApplicationDelegate_DidResignActive(_go, notification);
}

- (void) applicationWillUpdate:(NSNotification *)notification 
{
	_ApplicationDelegate_WillUpdate(_go, notification);
}

- (void) applicationDidUpdate:(NSNotification *)notification 
{
	_ApplicationDelegate_DidUpdate(_go, notification);
}

- (void) applicationWillTerminate:(NSNotification *)notification 
{
	_ApplicationDelegate_WillTerminate(_go, notification);
}

- (void) applicationDidChangeScreenParameters:(NSNotification *)notification 
{
	_ApplicationDelegate_DidChangeScreenParameters(_go, notification);
}

- (void) applicationDidChangeOcclusionState:(NSNotification *)notification 
{
	_ApplicationDelegate_DidChangeOcclusionState(_go, notification);
}
@end

void * newGoApplicationDelegate(void * go) {
	return [[GoApplicationDelegate alloc] init:go];
}
