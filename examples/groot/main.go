package main

/*
#cgo CFLAGS: -x objective-c
#cgo LDFLAGS: -framework Cocoa
#import <AppKit/AppKit.h>

void * newOwner();
void * getGrootWindow(void * owner);
void * getGrootView(void * owner);
void * getMainMenu(void * owner);

// void debug(void * id) {
// 	NSMenu * menu = id;
// 	NSLog(@"%@ %x %@", menu.className, menu, menu.title);
// }
*/
import "C"

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"unsafe"

	app "gitlab.com/firelizzard/go-app"
	"gitlab.com/firelizzard/go-app/cgo"
	"gitlab.com/firelizzard/go-app/darwin"
	"gitlab.com/firelizzard/go-app/event"
)

//go:generate ibtool groot.xib --compile groot.xib.bin

var window *app.Window

func load(s *app.Settings) {
	wd, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	owner := C.newOwner()
	defer darwin.Release(owner)

	ok := darwin.LoadNibFromFile(nil, filepath.Join(wd, "groot.xib.bin")).InstantiateAndRelease(owner, func(*darwin.Nib) {
		s.Menu = darwin.MenuFrom(C.getMainMenu(owner), cgo.GlobalScope)
		prefs := s.Menu.GetItem(0).Submenu().GetItem(2)
		if prefs.Title() != "Preferences…" {
			log.Fatalf("Menu 0 item 2 is not preferences: %q", prefs.Title())
		}

		s.Preferences = darwin.WindowFrom(C.getGrootWindow(owner))
		s.Preferences.Close()

		prefs.SetAction(func(unsafe.Pointer) {
			s.Preferences.Center()
			s.Preferences.Focus()
		})
	})
	if !ok {
		fmt.Fprintf(os.Stderr, "Failed to load groot.xib.bin\n")
		app.Terminate()
	}

	window = app.NewWindow(&app.WindowSettings{
		Title:        "I am Groot",
		Width:        640,
		Height:       480,
		Resizeable:   true,
		Minimizeable: true,
		Closeable:    true,
		ContentView:  C.getGrootView(owner),
	})

	window.SetDelegate(&darwin.WindowDelegate{
		WillClose: func(unsafe.Pointer) { app.Stop() },
	})
}

func main() {
	s := &app.Settings{
		AppName: "Groot",
	}
	load(s)

	darwin.SetDelegate(&darwin.ApplicationDelegate{
		ShouldTerminate: func(unsafe.Pointer) darwin.NSApplicationTerminateReply {
			fmt.Println("Should terminate?")
			return darwin.TerminateNow
		},
	})

	app.Start(s)

	v := make(chan interface{})
	event.Notify(v, event.Alive)
	go func() {
		<-v
		app.Dispatch(func() {
			window.Center()
			window.Focus()
		})

		for range v {
		}
	}()

	app.Run()
}
