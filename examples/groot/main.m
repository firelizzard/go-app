#import <AppKit/AppKit.h>

@interface InterfaceOwner : NSObject
@property (retain) IBOutlet NSWindow * grootWindow;
@property (retain) IBOutlet NSView * grootView;
@property (retain) IBOutlet NSMenu * mainMenu;
@end

@implementation InterfaceOwner
- (void) dealloc
{
    self.grootWindow = nil;
    self.grootView = nil;
    self.mainMenu = nil;
    [super dealloc];
}
@end

void * newOwner() {
    return [InterfaceOwner new];
}

void * getGrootWindow(void * owner) {
    return [(InterfaceOwner *)owner grootWindow];
}

void * getGrootView(void * owner) {
    return [(InterfaceOwner *)owner grootView];
}

void * getMainMenu(void * owner) {
    return [(InterfaceOwner *)owner mainMenu];
}