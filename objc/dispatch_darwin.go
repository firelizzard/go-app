/*
	Copyright (c) 2019 The Go App Authors

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

package objc

/*
#import <AppKit/AppKit.h>
#include <pthread.h>

static uint64_t threadID() {
	uint64_t id;
	if (pthread_threadid_np(pthread_self(), &id)) {
		abort();
	}
	return id;
}

void dispatchExecute(void *);

static void dispatch(void * fn) {
    dispatch_async_f(dispatch_get_main_queue(), fn, dispatchExecute);
}
*/
import "C"

import (
	"runtime"
	"unsafe"

	"gitlab.com/firelizzard/go-app/cgo"
)

var mainThreadID = C.threadID()

//export dispatchExecute
func dispatchExecute(fn unsafe.Pointer) {
	ref := cgo.Reference(fn)
	ref.Load().(func())()
	ref.Delete()
}

// Dispatch will execute `fn` on the main thread.
//
// If Dispatch is called on the main thread, it will execute synchronously.
func Dispatch(fn func()) {
	runtime.LockOSThread()
	defer runtime.UnlockOSThread()

	if IsMainThread() {
		fn()
	} else {
		C.dispatch(cgo.Save(fn).C())
	}
}

// IsMainThread returns whether the current thread is the main thread.
//
// To avoid race conditions, the calling routine must call
// `runtime.LockOSThread()` (and `runtime.UnlockOSThread()`).
func IsMainThread() bool {
	return C.threadID() == mainThreadID
}
