/*
	Copyright (c) 2019 The Go App Authors

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

package objc

/*
#import <objc/runtime.h>
#import <AppKit/AppKit.h>

static bool is(void * self, const char * clsname) {
	Class cls = objc_getClass(clsname); free((void *)clsname);
	if (!cls)
		return NO;

	return [(id<NSObject>)self isKindOfClass:cls];
}

static const char * getClassName(void * self) {
	return class_getName(((id<NSObject>)self).class);
}

static void release(void * self) {
	[(id)self release];
}
*/
import "C"

import (
	"fmt"
	"unsafe"
)

func IsClass(obj unsafe.Pointer, cls string) bool {
	return bool(C.is(obj, C.CString(cls)))
}

func AssertIsClass(obj unsafe.Pointer, cls string) {
	if IsClass(obj, cls) {
		return
	}

	is := C.GoString(C.getClassName(obj))
	panic(fmt.Errorf("expected object isa %s, got %s", cls, is))
}

// Release calls `[obj release]`
func Release(obj unsafe.Pointer) {
	C.release(obj)
}

// FilterClass returns a filter that checks if an object is an instance of the
// specified class.
func FilterClass(cls string) func(unsafe.Pointer) bool {
	return func(obj unsafe.Pointer) bool {
		return IsClass(obj, cls)
	}
}
