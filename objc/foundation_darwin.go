/*
	Copyright (c) 2019 The Go App Authors

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

package objc

/*
#import <stdint.h>
#import <AppKit/AppKit.h>

static void * allocString(void * bytes, NSUInteger len) {
	return [[NSString alloc] initWithBytesNoCopy:bytes length:len encoding:NSUTF8StringEncoding freeWhenDone:NO];
}

static void * allocData(void * bytes, NSUInteger len) {
	return [[NSData alloc] initWithBytes:bytes length:len];
}

static void * allocURL(void * str) {
	return [[NSURL alloc] initWithString:(NSString *)str];
}

static void * allocDataFromFile(void * path) {
	NSData * data = [[NSData alloc] initWithContentsOfFile:path];
	[(id)path release];
	return data;
}

static NSUInteger count(void * id) {
	return ((NSArray *)id).count;
}

static NSUInteger length(void * id) {
	return ((NSData *)id).length;
}

static void * getr(void * id, NSUInteger index) {
	return ((NSArray *)id)[index];
}

static void * getd(void * id, void * key) {
	return ((NSDictionary *)id)[key];
}

static void * valueForKey(void * self, void * key) {
	return [(NSObject *)self valueForKeyPath:(NSString *)key];
}

static void setValueForKey(void * self, void * value, void * key) {
	[(NSObject *)self setValue:(id)value forKeyPath:(NSString *)key];
}

static const char * goString(void * self) {
	return [(NSString *)self UTF8String];
}

static void getBytes(void * self, void * buffer, NSUInteger length) {
	[(NSData *)self getBytes:buffer length:length];
}

static void * allocNumberWithBool(bool v)         { return [NSNumber numberWithBool:v]; }
static void * allocNumberWithInt(int v)           { return [NSNumber numberWithInt:v]; }
static void * allocNumberWithUint(unsigned int v) { return [NSNumber numberWithUnsignedInt:v]; }
static void * allocNumberWithI8(int8_t v)         { return [NSNumber numberWithChar:v]; }
static void * allocNumberWithI16(int16_t v)       { return [NSNumber numberWithShort:v]; }
static void * allocNumberWithI32(int32_t v)       { return [NSNumber numberWithLong:v]; }
static void * allocNumberWithI64(int64_t v)       { return [NSNumber numberWithLongLong:v]; }
static void * allocNumberWithU8(uint8_t v)        { return [NSNumber numberWithUnsignedChar:v]; }
static void * allocNumberWithU16(uint16_t v)      { return [NSNumber numberWithUnsignedShort:v]; }
static void * allocNumberWithU32(uint32_t v)      { return [NSNumber numberWithUnsignedLong:v]; }
static void * allocNumberWithU64(uint64_t v)      { return [NSNumber numberWithUnsignedLongLong:v]; }
static void * allocNumberWithF32(float v)         { return [NSNumber numberWithFloat:v]; }
static void * allocNumberWithF64(double v)        { return [NSNumber numberWithDouble:v]; }

static NSInteger streamRead(void * stream, uint8_t * buf, NSUInteger len) {
	return [(NSInputStream *)stream read:buf maxLength:len];
}
*/
import "C"

import (
	"errors"
	"fmt"
	"io"
	"runtime"
	"unsafe"
)

// ValueForKey calls `[obj valueForKey:key]`
func ValueForKey(obj unsafe.Pointer, key string) unsafe.Pointer {
	k := NSString(key)
	defer Release(k)
	return C.valueForKey(obj, k)
}

func SetValueForKey(obj, val unsafe.Pointer, key string) {
	k := NSString(key)
	defer Release(k)
	C.setValueForKey(obj, val, k)
}

// NSString converts a string to an NSString, which must be manually released
func NSString(s string) unsafe.Pointer {
	b := []byte(s)
	return C.allocString(unsafe.Pointer(&b[0]), C.NSUInteger(len(b)))
}

// GoString converts an NSString to a string
func GoString(id unsafe.Pointer) string {
	if id == nil {
		return ""
	}

	AssertIsClass(id, "NSString")
	return C.GoString(C.goString(id))
}

// NSURL creates an NSURL, which must be manually released, from a string
func NSURL(s string) unsafe.Pointer {
	t := NSString(s)
	defer Release(t)
	return C.allocURL(t)
}

// NSData converts a byte slice to NSData, which must be manually released
func NSData(b []byte) unsafe.Pointer {
	return C.allocData(unsafe.Pointer(&b[0]), C.NSUInteger(len(b)))
}

// NSData converts a byte slice to NSData, which must be manually released
func NSDataFromFile(path string) unsafe.Pointer {
	return C.allocDataFromFile(NSString(path))
}

// GoBytes converts NSData to a byte slice
func GoBytes(id unsafe.Pointer) []byte {
	if id == nil {
		return nil
	}

	AssertIsClass(id, "NSData")
	l := C.length(id)
	b := make([]byte, int(l))
	C.getBytes(id, unsafe.Pointer(&b[0]), l)
	return b
}

// GoReader wraps an NSInputStream with a Go reader
func GoReader(id unsafe.Pointer, releaseWhenFinalized bool) io.Reader {
	if id == nil {
		return nil
	}

	AssertIsClass(id, "NSInputStream")
	r := ReaderFunc(func(p []byte) (int, error) {
		n := C.streamRead(id, (*C.uint8_t)(&p[0]), C.NSUInteger(len(p)))
		if n == 0 {
			return 0, io.EOF
		}
		if n < 0 {
			return int(n), errors.New("An NSInputStream error occured")
		}
		return int(n), nil
	})
	if releaseWhenFinalized {
		runtime.SetFinalizer(r, func(ReaderFunc) {
			Release(id)
		})
	}
	return r
}

type ReaderFunc func([]byte) (n int, err error)

func (r ReaderFunc) Read(p []byte) (n int, err error) {
	return r(p)
}

// NSNumber converts a numeric or boolean value to an NSNumber, which must be manually released
func NSNumber(v interface{}) unsafe.Pointer {
	switch v := v.(type) {
	case bool:
		return C.allocNumberWithBool(C.bool(v))
	case int:
		return C.allocNumberWithInt(C.int(v))
	case uint:
		return C.allocNumberWithUint(C.uint(v))
	case int8:
		return C.allocNumberWithI8(C.int8_t(v))
	case int16:
		return C.allocNumberWithI16(C.int16_t(v))
	case int32:
		return C.allocNumberWithI32(C.int32_t(v))
	case int64:
		return C.allocNumberWithI64(C.int64_t(v))
	case uint8:
		return C.allocNumberWithU8(C.uint8_t(v))
	case uint16:
		return C.allocNumberWithU16(C.uint16_t(v))
	case uint32:
		return C.allocNumberWithU32(C.uint32_t(v))
	case uint64:
		return C.allocNumberWithU64(C.uint64_t(v))
	case float32:
		return C.allocNumberWithF32(C.float(v))
	case float64:
		return C.allocNumberWithF64(C.double(v))
	default:
		panic(fmt.Errorf("invalid type for conversion to NSNumber: %T", v))
	}
}

// EnumerateDictionary enumerates the keys and values of an NSDictionary
func EnumerateDictionary(id unsafe.Pointer, fn func(k, v unsafe.Pointer)) {
	if id == nil {
		return
	}

	AssertIsClass(id, "NSDictionary")

	enum := ValueForKey(id, "keyEnumerator")
	for {
		key := ValueForKey(enum, "nextObject")
		if key == nil {
			break
		}
		fn(key, C.getd(id, key))
	}
}

type Array struct {
	id unsafe.Pointer
}

func ArrayFrom(id unsafe.Pointer) *Array {
	return &Array{id}
}

// Release releases the array.
func (r *Array) Release() {
	Release(r.id)
	r.id = nil
}

// Autorelease sets the finalizer for the go object to Release.
func (r *Array) Autorelease() {
	runtime.SetFinalizer(r, (*Array).Release)
}

// Len returns the length of the array.
func (r *Array) Len() uint {
	return uint(C.count(r.id))
}

// Get returns the i'th element of the array.
func (r *Array) Get(i uint) unsafe.Pointer {
	return C.getr(r.id, C.NSUInteger(i))
}

// Find returns the first element of the array for which all functions return true.
func (r *Array) Find(fns ...func(unsafe.Pointer) bool) (int, unsafe.Pointer) {
	filter := func(obj unsafe.Pointer) bool {
		for _, fn := range fns {
			if !fn(obj) {
				return false
			}
		}
		return true
	}

	N := r.Len()
	for i := uint(0); i < N; i++ {
		obj := r.Get(i)
		if filter(obj) {
			return int(i), obj
		}
	}
	return -1, nil
}
